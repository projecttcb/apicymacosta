<?php

use App\User;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Contracts\Permission;
use Spatie\Permission\Models\Permission as ModelsPermission;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('permissions')->insert([
            'name'       => 'Visualizar',
            'guard_name' => 'web',
            'created_at' => date('Y-m-d H:i:s'),            
            'updated_at' => date('Y-m-d H:i:s'),
        ]);
        
        DB::table('permissions')->insert([
            'name'       => 'Crear',
            'guard_name' => 'web',
            'created_at' => date('Y-m-d H:i:s'),            
            'updated_at' => date('Y-m-d H:i:s'),
        ]);

        DB::table('permissions')->insert([
            'name'       => 'Editar',
            'guard_name' => 'web',
            'created_at' => date('Y-m-d H:i:s'),            
            'updated_at' => date('Y-m-d H:i:s'),
        ]);
        
        DB::table('permissions')->insert([
            'name'       => 'Eliminar',
            'guard_name' => 'web',
            'created_at' => date('Y-m-d H:i:s'),            
            'updated_at' => date('Y-m-d H:i:s'),
        ]);
        
        DB::table('permissions')->insert([
            'name'       => 'Visualizar.Admin',
            'guard_name' => 'web',
            'created_at' => date('Y-m-d H:i:s'),            
            'updated_at' => date('Y-m-d H:i:s'),
        ]);
        
        DB::table('permissions')->insert([
            'name'       => 'Crear.Admin',
            'guard_name' => 'web',
            'created_at' => date('Y-m-d H:i:s'),            
            'updated_at' => date('Y-m-d H:i:s'),
        ]);

        DB::table('permissions')->insert([
            'name'       => 'Editar.Admin',
            'guard_name' => 'web',
            'created_at' => date('Y-m-d H:i:s'),            
            'updated_at' => date('Y-m-d H:i:s'),
        ]);

        DB::table('permissions')->insert([
            'name'       => 'Eliminar.Admin',
            'guard_name' => 'web',
            'created_at' => date('Y-m-d H:i:s'),            
            'updated_at' => date('Y-m-d H:i:s'),
        ]);
        
        DB::table('permissions')->insert([
            'name'       => 'Visualizar.Permiso',
            'guard_name' => 'web',
            'created_at' => date('Y-m-d H:i:s'),            
            'updated_at' => date('Y-m-d H:i:s'),
        ]);
        
        DB::table('permissions')->insert([
            'name'       => 'Crear.Permiso',
            'guard_name' => 'web',
            'created_at' => date('Y-m-d H:i:s'),            
            'updated_at' => date('Y-m-d H:i:s'),
        ]);

        DB::table('permissions')->insert([
            'name'       => 'Editar.Permiso',
            'guard_name' => 'web',
            'created_at' => date('Y-m-d H:i:s'),            
            'updated_at' => date('Y-m-d H:i:s'),
        ]);

        DB::table('permissions')->insert([
            'name'       => 'Eliminar.Permiso',
            'guard_name' => 'web',
            'created_at' => date('Y-m-d H:i:s'),            
            'updated_at' => date('Y-m-d H:i:s'),
        ]);
        
        DB::table('permissions')->insert([
            'name'       => 'Visualizar.Perfil',
            'guard_name' => 'web',
            'created_at' => date('Y-m-d H:i:s'),            
            'updated_at' => date('Y-m-d H:i:s'),
        ]);
        DB::table('permissions')->insert([
            'name'       => 'Crear.Perfil',
            'guard_name' => 'web',
            'created_at' => date('Y-m-d H:i:s'),            
            'updated_at' => date('Y-m-d H:i:s'),
        ]);
        DB::table('permissions')->insert([
            'name'       => 'Editar.Perfil',
            'guard_name' => 'web',
            'created_at' => date('Y-m-d H:i:s'),            
            'updated_at' => date('Y-m-d H:i:s'),
        ]);
        DB::table('permissions')->insert([
            'name'       => 'Eliminar.Perfil',
            'guard_name' => 'web',
            'created_at' => date('Y-m-d H:i:s'),            
            'updated_at' => date('Y-m-d H:i:s'),
        ]);

        DB::table('roles')->insert([
            'name'       => 'SuperAdmin',
            'guard_name' => 'web',
            'created_at' => date('Y-m-d H:i:s'),            
            'updated_at' => date('Y-m-d H:i:s'),
        ]);

        $user = User::create([
            'name' => 'Marcelino',
            'last_name' => 'Mejia',
            'email' => 'mmejia1@tecbaco.com',
            'password_temp' => 0,
            'password' => Hash::make('Tecbaco2021*'),
        ]);
        
       
        $rol = Role::where('name', 'SuperAdmin')->get();
        $user->syncRoles($rol);

        $permi = ModelsPermission::all();
        foreach ($permi as $per) {
            $per->assignRole($rol);
           // $user->givePermissionTo($per->id);
        }
        
    }
}
