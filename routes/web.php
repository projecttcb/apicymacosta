<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
    //redirect()->action('AdminController@index');
});

Route::get('/email', function () {
    return view('email.email');
});

Auth::routes();

//Route::get('/home', 'HomeController@index', 'permission:Visualizar|Crear|Editar|Eliminar')->name('home');
Route::get('/ajaxAdmin/{id}', 'AdminController@ajaxAdmin')->name('admin.ajaxAdmin');
Route::middleware(['auth', 'PasswordTemporary'])->group(function () {
    Route::get('/admin', 'AdminController@index')->name('admin.index')->middleware('permission:Visualizar|Crear|Editar|Eliminar|Visualizar.Admin|Crear.Admin|Editar.Admin|Eliminar.Admin');
    Route::get('/admin/create', 'AdminController@create')->name('admin.create')->middleware(['permission:Crear|Crear.Admin']);
    Route::post('/admin/create', 'AdminController@store')->name('admin.store')->middleware(['permission:Crear|Crear.Admin']);
    Route::get('/admin/edit/{id}', 'AdminController@edit')->name('admin.edit')->middleware(['permission:Editar|Editar.Admin']);
    Route::put('/admin/{id}', 'AdminController@update')->name('admin.update')->middleware(['permission:Editar|Editar.Admin']);
    Route::delete('/admin/{id}', 'AdminController@destroy')->name('admin.destroy')->middleware(['permission:Eliminar|Eliminar.Admin']);
    Route::get('/ajaxAdmin/{id}', 'AdminController@ajaxAdmin')->name('admin.ajaxAdmin')->middleware('permission:Visualizar|Visualizar.Admin');

    Route::get('/permission', 'PermissionsController@index')->name('permission.index')->middleware('permission:Visualizar|Crear|Editar|Eliminar|Visualizar.Permiso|Crear.Permiso|Editar.Permiso|Eliminar.Permiso');
    Route::get('/permission/create', 'PermissionsController@create')->name('permission.create')->middleware(['permission:Crear|Crear.Permiso']);
    Route::post('/permission/create', 'PermissionsController@store')->name('permission.store')->middleware(['permission:Crear|Crear.Permiso']);
    Route::get('/permission/edit/{id}', 'PermissionsController@edit')->name('permission.edit')->middleware(['permission:Editar|Editar.Permiso']);
    Route::put('/permission/{id}', 'PermissionsController@update')->name('permission.update')->middleware(['permission:Editar|Editar.Permiso']);
    Route::delete('/permission/{id}', 'PermissionsController@destroy')->name('permission.destroy')->middleware(['permission:Eliminar|Eliminar.Permiso']);
    Route::get('/ajaxPermission/{id}', 'PermissionsController@ajaxPermission')->name('permission.ajaxPermission')->middleware('permission:Visualizar|Visualizar.Permiso');

    Route::get('/module', 'ModuleController@index')->name('module.index')->middleware('permission:Visualizar|Crear|Editar|Eliminar|Visualizar.Modulo|Crear.Modulo|Editar.Modulo|Eliminar.Modulo');;
    Route::get('/module/create', 'ModuleController@create')->name('module.create')->middleware(['permission:Crear|Crear.Modulo']);
    Route::post('/module/create', 'ModuleController@store')->name('module.store')->middleware(['permission:Crear|Crear.Modulo']);
    Route::get('/module/edit/{id}', 'ModuleController@edit')->name('module.edit')->middleware(['permission:Editar|Editar.Modulo']);
    Route::put('/module/edit/{id}', 'ModuleController@update')->name('module.update')->middleware(['permission:Editar|Editar.Modulo']);
    Route::delete('/module/{id}', 'ModuleController@destroy')->name('module.destroy')->middleware(['permission:Eliminar|Eliminar.Modulo']);
    Route::get('/ajaxModule/{id}', 'ModuleController@ajaxModule')->name('module.ajaxModule');

    Route::get('/perfil', 'PerfilController@index')->name('perfil.index')->middleware('permission:Visualizar|Crear|Editar|Eliminar|Visualizar.Perfil|Crear.Perfil|Editar.Perfil|Eliminar.Perfil');
    Route::get('/perfil/create', 'PerfilController@create')->name('perfil.create')->middleware(['permission:Crear|Crear.Perfil']);
    Route::post('/perfil/create', 'PerfilController@store')->name('perfil.store')->middleware(['permission:Crear|Crear.Perfil']);
    Route::get('/perfil/edit/{id}', 'PerfilController@edit')->name('perfil.edit')->middleware(['permission:Editar|Editar.Perfil']);
    Route::put('/perfil/{id}', 'PerfilController@update')->name('perfil.update')->middleware(['permission:Editar|Editar.Perfil']);
    Route::delete('/perfil/{id}', 'PerfilController@destroy')->name('perfil.destroy')->middleware(['permission:Eliminar|Eliminar.Perfil']);
    Route::get('/ajaxRoles/{id}', 'PerfilController@ajaxRoles')->name('perfil.ajaxRoles')->middleware('permission:Visualizar|Visualizar.Perfil');

    //perfil user
    Route::get('/userperfil/{id}', 'UserPerfilController@show')->name('userperfil.show');
    Route::put('/userperfil/{id}', 'UserPerfilController@update')->name('userperfil.update');
    
});


//Google
Route::get('auth/google', 'Auth\GoogleController@redirectToGoogle');
Route::get('auth/google/callback', 'Auth\GoogleController@handleGoogleCallback');



