<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class PasswordTemporary
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        //verificar que los que el usuario halla cambiado la contraseña    
        if( Auth::check() ){
            if(Auth::user()->password_temp == 1){    
              return redirect('/password/reset');
            }
        }

        return $next($request);
    }
}
