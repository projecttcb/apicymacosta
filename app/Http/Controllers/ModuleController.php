<?php

namespace App\Http\Controllers;

use App\Module;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Traits\HasRoles;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Traits\HasPermissions;

class ModuleController extends Controller
{
    use HasPermissions;
    use HasRoles;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $module = Module::all();
        return view('admin.module.index', compact('module'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $permission = Permission::all();
        $roles = Role::all();
        return view('admin.module.create', compact('permission', 'roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            'idModule' => 'nullable|unique:modules|max:3',
            'module'   => 'required',
            'tipo'     => 'required',
        ]);
        
        $module = new Module();
        $module->idModule = $data['idModule'];
        $module->module   = $data['module'];
        $module->tipo     = $data['tipo'];
        $module->save();

        $module->syncPermissions($request->permission_id);

        $module->syncRoles($request->perfil_id);
        
        return redirect()->action('ModuleController@index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Module  $module
     * @return \Illuminate\Http\Response
     */
    public function show(Module $module)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Module  $module
     * @return \Illuminate\Http\Response
     */
    public function edit( $id)
    {
        $module = Module::findOrFail($id);
        $mod = array($module->tipo);
        $permission = Permission::all();
        $roles = Role::all();
        $idPermission = array();
        $idRoles = array();

        foreach ($module->permissions as $key => $value) {
            array_push($idPermission, $value->id);
        }
        
        foreach ($module->roles as $key => $value) {
            array_push($idRoles, $value->id);
        }

        //return $idPermission;
        return view('admin.module.edit', compact('permission', 'roles', 'idPermission', 'idRoles', 'mod', 'module'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Module  $module
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //return $request->all();
        $module = Module::findOrFail($id);
        
        $data = $request->validate([
            'idModule' => 'nullable|max:3',
            'module'   => 'required',
            'tipo'     => 'required',
        ]);

        $module->idModule = $data['idModule'];
        $module->module   = $data['module'];
        $module->tipo     = $data['tipo'];
        $module->save();

        $module->syncPermissions($request->permission_id);
    
        $module->syncRoles($request->perfil_id);
    
        
        return redirect()->action('ModuleController@index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Module  $module
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $module = Module::findOrFail($id);

        $module->delete();
        
        return redirect()->action('ModuleController@index')->with('eliminar', 'ok');
    }

    public function ajaxModule($id)
    {
        $module = Module::findOrfail($id);
        $idPermission = array();
        $idRol = array();

        foreach ($module->permissions as $value) {
            array_push($idPermission, $value->name);
        }
        
        foreach ($module->roles as $value) {
            array_push($idRol, $value->name);
        }

        return json_encode(['module' => $module->module, 'permission' => $idPermission, 'role'=> $idRol]);
          
    }
}
