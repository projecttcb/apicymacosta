<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Laravel\Socialite\Facades\Socialite;
use Laravel\Socialite\Two\InvalidStateException;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class GoogleController extends Controller
{
    use AuthenticatesUsers;

    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    public $maxAttempts = 3;

    /**
     * Set how many seconds a lockout will last.
     */
    public $decayMinutes = 5;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
    
    public function redirectToGoogle()
    {
        return Socialite::driver('google')->redirect();
    }

    public function handleGoogleCallback()
    {
        try {
            $user = Socialite::driver('google')->stateless()->user();  
            $findUser = User::where('email', $user->email)->first();
            
            if($findUser){
                Auth::login($findUser);
                $findUser->google_id = $user->id;
                $findUser->avatar = $user->getAvatar();
                $findUser->save();
                return redirect('/admin');

                /* auth()->login($findUser, true);
                $findUser->google_id = $user->id;
                $findUser->save();
                return redirect($this->redirectPath()); */

            }else{
                /* $newUser = User::create([
                    'name' => $user->name,
                    'email' => $user->email,
                    'google_id'=> $user->id,
                    'password' => Hash::make($user->password),
                ]);
    
                Auth::login($newUser); */
     
                return redirect('/login')->with('error', 'ok');
            }
        } catch (InvalidStateException $th) {
            //print_r($th);
            dd($th);
        }

    }
}
