<?php

namespace App\Http\Controllers;

use App\User;
use App\AuditPassword;
use Illuminate\Http\Request;
use App\Rules\PasswordStrong;
use App\Rules\Splitstringarrays;
use Illuminate\Support\Facades\Hash;
use App\Rules\Oldpassword;

class UserPerfilController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::findOrFail($id);

        return view('UserPerfil.userperfil', compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //return $request;
        $usuario = User::findOrFail($id);
        // return array($request->roles_id);
         $data = $request->validate([
             'name' => 'required',
             'last_name' => 'required',
         ]);
        
        
         $usuario->name = $data['name'];
         $usuario->last_name = $data['last_name'];
         $usuario->save();
 
         if ($request->permission_id) {
             $usuario->givePermissionTo($request->permission_id);
         }
 
         if ($request->password && $request->password_confirmation) {           
            
             $data = $request->validate([
                 'password' => ['required','string','min:8','confirmed','not_regex:/Tecbaco/',
                 new PasswordStrong(), 
                 new Splitstringarrays(),
                 new Oldpassword(),]
             ]);
             $usuario->password = Hash::make($data['password']);
             $usuario->save(); 
 
             $passAudit = new AuditPassword();
             $passAudit->password = $usuario->password;
             $passAudit->user_id = $usuario->id;
             $passAudit->save();
                         
         }
 
         return back()->with('actualizar', 'ok');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
