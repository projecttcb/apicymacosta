<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;


class PerfilController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $profiles = Role::all();

        $permission = Permission::select(['name'])->get();
        $permissionName = array();
        foreach ($permission as $key => $value) {
            array_push($permissionName, $value->name);
        }

        return view('admin.perfil.index', compact('profiles', 'permissionName'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $permission = Permission::all(); 
        $users = User::all(); 
        return view('admin.perfil.create', compact('permission','users'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $data = $request->validate([
            'name' => 'required',
            'user_id' => 'nulleable',
            'permisssion_id' => 'nulleable',
        ]); 

        $role = Role::create(['name' => $data['name'], 'guard_name'=>'web']);
        $role->save();

        $role->syncPermissions($request->permission_id);
        
        return redirect()->action('PerfilController@index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $profiles  = Role::findOrFail($id);
        $permission = Permission::all();
        $users = User::all();

        $permissionId = array();
        $userId = array();

        foreach ($profiles->permissions as $value) {
            array_push($permissionId, $value->id);
        }
        
        foreach ($profiles->users as $per) {
            array_push($userId, $per->id);
        }

       // return $permissionId;
        return view('admin.perfil.edit', compact('profiles', 'permission', 'users', 'permissionId', 'userId'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rol = Role::findOrFail($id);
         
        $rol->name = $request->name;
        $rol->guard_name = 'web';
        $rol->save();

        $rol->syncPermissions($request->permission_id);


        return redirect()->action('PerfilController@index');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $rol = Role::findOrFail($id);

        $rol->delete();

        return redirect()->action('PerfilController@index')->with('eliminar', 'ok');   
    }


    public function ajaxRoles($id)
    {
        $rol = Role::findOrFail($id);   
        $idPermission = array();

        foreach ($rol->permissions as $value) {
            array_push($idPermission, $value->name);
        }

        return json_encode(['rol' => $rol->name, 'permission' => $idPermission]);
    }
}
