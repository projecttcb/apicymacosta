<?php

namespace App\Http\Controllers;

use App\User;
use Carbon\Carbon;
use App\Mail\SendEmail;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Mail;
use Spatie\Permission\Models\Permission;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::all();
        $permission = Permission::select(['name'])->get();
        $permissionName = array();
        foreach ($permission as $key => $value) {
            array_push($permissionName, $value->name);
        }
        //return $permissionName;
        
        return view('admin.index', compact('users', 'permissionName'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $permission = Permission::all();
        $roles = Role::all();
        return view('admin.create', compact('permission', 'roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         //return $request->all(); 
         $data = $request->validate([
            'name'                  => ['required', 'string', 'max:255'],
            'last_name'             => ['required', 'string', 'max:255'],
            'email'                 => ['required', 'string', 'email', 'max:255', 'unique:users'],
            //'password'              => ['required', 'string', 'min:8', 'confirmed'],
            //'password_confirmation' => ['required'],
        ]);

        $user = new User();  
        $pass = 'Tecbaco'.Carbon::now()->year.'*';
        $user->name = $data['name'];
        $user->last_name = $data['last_name'];
        $user->email = $data['email'];
        $user->password = bcrypt($pass);
        $user->password_temp = 1;
        //return $user->password_temp;
        //$user->password = Hash::make($data['password']);
        $user->save();

        Mail::to($request->email)->send(new SendEmail ($user->name, $user->email, $pass));
           
        $user->syncPermissions($request->permission_id);
    
        $user->syncRoles($request->perfil_id);

        return redirect()->action('AdminController@index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::findOrFail($id);
        $permission = Permission::all();
        $roles = Role::all();
        $rolesId = array();
        $permissionId = array();

        $passTempo =  $user->password_temp;
        foreach ($user->roles as $key => $rol) {
            array_push($rolesId, $rol->id );
        }
        foreach ($user->permissions as $key => $per) {
            array_push($permissionId, $per->id );
        }

        return view('admin.edit', compact('user', 'permission', 'roles', 'permissionId', 'rolesId', 'passTempo'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::findOrFail($id);
        
        $data = $request->validate([
            'name' => 'required',
            'last_name' => 'required',
            'email' => 'required | string | email | max:255',
        ]);
        
        $user->name = $data['name'];
        $user->last_name = $data['last_name'];
        $user->email = $data['email'];
        
        if ($request->password_temp) {
            $user->password = bcrypt('Tecbaco'.Carbon::now()->year.'*');
            $user->password_temp = $request->password_temp;
        } else {
            $user->password_temp = 0;
        }
        
        $user->save();
        

        $user->syncPermissions($request->permission_id);
    
        $user->syncRoles($request->perfil_id);
        

        return redirect()
                ->action('AdminController@index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::findOrFail($id);
        $user->delete();

        return redirect()->action('AdminController@index')->with('eliminar', 'ok');
    }
    public function ajaxAdmin($id)
    {
        $user = User::findOrFail($id);
        $idPermission = array();
        $idPerfil = array();

        foreach ($user->permissions as $value) {
            array_push($idPermission, $value->name);
        }
        
        foreach ($user->roles as $value) {
            array_push($idPerfil, $value->name);
        }

        return json_encode(['user' => $user->name, 'permission' => $idPermission, 'role'=>$idPerfil]);
    }
}
