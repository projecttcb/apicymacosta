<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class PermissionsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $permissions = Permission::all();
        $permission = Permission::select(['name'])->get();
        $permissionName = array();
        foreach ($permission as $key => $value) {
            array_push($permissionName, $value->name);
        }
        return view('admin.permission.index', compact('permissions','permissionName'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $users = User::all();
        $roles = Role::all();
        return view('admin.permission.create', compact('users', 'roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $data = $request->validate([
            'name' => 'required',
            'user_id' => 'nullable',
        ]); 


        $permission = Permission::create( [ 'name' => $data['name'], 'guard_name'=>'web'] );
        $permission->save();

        if ($request->perfil) {
            $permission->syncRoles($request->perfil);
        }    

        
        if ($request->user_id) {
            foreach ( $request->user_id as $userId ) {
                $usr = User::findOrFail( $userId );
                $usr->givePermissionTo( $permission->id );
            }
        }

        

        

        return redirect()->action('PermissionsController@index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $userId = array();
        $rolesId = array();
        $users = User::all();
        $roles = Role::all();
        $permission = Permission::findOrFail($id);
       
        foreach ($permission->users as $per) {
            array_push($userId, $per->id);
        }
        
        foreach ($permission->roles as $per) {
            array_push($rolesId, $per->id);
        }



        return view('admin.permission.edit', compact('permission', 'users', 'roles', 'userId', 'rolesId'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //return $request;
        $permission = Permission::findOrfail($id);
        $permission->name = $request->name; 
        $permission->guard_name = 'web'; 
        $permission->save();

        
        $permission->syncRoles($request->perfil);

        if($request->user_id ){
            foreach ( $request->user_id as $userId ) {
                $usr = User::findOrFail( $userId );
                $usr->syncPermissions( $permission->id );
            }
        }
        
        

        return redirect()->action('PermissionsController@index');


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        
        $permission = Permission::findOrfail($id);
        
        $permission->delete();
    
        //return json_decode();
        return redirect()->action('PermissionsController@index')->with('eliminar', 'ok'); 
    }

    public function ajaxPermission($id)
    {
        $permission = Permission::findOrFail($id);
        $idRoles = array();

        foreach ($permission->roles as $value) {
            array_push($idRoles, $value->name);
        }
        
        return json_encode(['permission' => $permission->name, 'roles' => $idRoles]);
    }
}
