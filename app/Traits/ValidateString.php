<?php

namespace App\Traits; 

trait ValidateString{
     /**
     * devuelve una cadena partida en grupos de tres
     * caracteres juntos para validar si la contraseña
     * cumple con esta politica.
     *
     * @param  string  str
     * @param  integer len_policy
     * @return array   str_split
    */
    public function validateSplitString($str, $len_policy = 0)
    {
        if ($len_policy > 0) {
            $str_split = array();
            $len_str = mb_strlen($str, "UTF-8");
            for ($i = 0; $i < $len_str; $i++) {
                if( ($len_str - $i) >= $len_policy )
                {
                    $str_split[] = mb_substr($str, $i, $len_policy, "UTF-8");
                }
            }
            return $str_split;
        }
    }
}


?>