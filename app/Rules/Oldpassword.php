<?php

namespace App\Rules;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Contracts\Validation\Rule;

class Oldpassword implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $user = Auth::user();
        $userPassAudit = DB::table('audit_passwords')
            ->select('password')
            ->where('user_id', '=', $user->id)
            ->orderBy('created_at', 'DESC')
            ->get();
        
        
          
      /*   $passAudit = array();
        foreach ($userPassAudit as  $dataValue) {
            array_push($passAudit, $dataValue);
        }
        // comprobamos que la contraseña no se haya utilizado antes
        $oldPass = in_array(bcrypt($value),  $passAudit);

        return $oldPass;
        */
        // comprobamos que la contraseña no se haya utilizado antes
        foreach ($userPassAudit as $userPassAnt) {
            if ( Hash::check($value, $userPassAnt->password) ) {
                return false;
            }
        }

        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'La contraseña no puede ser igual a una usada anteriormente.';
    }
}
