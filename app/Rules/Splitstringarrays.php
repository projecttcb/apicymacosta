<?php

namespace App\Rules;

use Illuminate\Support\Str;
use App\Traits\ValidateString;
use Illuminate\Support\Facades\Auth;
use Illuminate\Contracts\Validation\Rule;

class Splitstringarrays implements Rule
{
    use ValidateString;
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $user = Auth::user();
           
        // obtenemos conjuntos de caracteres de los campos primarios del usuario
        $splitStringArrays = array();
        array_push($splitStringArrays, $this->validateSplitString($user->name, 3));
        array_push($splitStringArrays, $this->validateSplitString($user->last_name, 3));

        // comprobamos que la contraseña no coincida con esos conjuntos de campos primarios
        foreach ($splitStringArrays as $array) {
            foreach ($array as $item) {  
                if (Str::contains(strtolower($value), strtolower($item))) {
                    return false;
                }
            }
        }

        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'La contraseña no puede contener partes o ser igual a su nombre, apellido o la empresa';
    }
}
