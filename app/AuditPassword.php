<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AuditPassword extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'password',
        'user_id'
    ];

    /**
    * Relacion de uno a muchos inversa con el modelo User
    */
    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
