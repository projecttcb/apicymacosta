<?php

namespace App;

use Spatie\Permission\Traits\HasRoles;
use Illuminate\Database\Eloquent\Model;
use Spatie\Permission\Traits\HasPermissions;
use Illuminate\Database\Eloquent\SoftDeletes;

class Module extends Model
{
    use HasPermissions;
    use HasRoles;
    use SoftDeletes;

    protected $guard_name = 'web';

    protected $fillable = [
        'module', 'activo', 'tipo',
    ];
}
