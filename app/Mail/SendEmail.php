<?php

namespace App\Mail;

use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SendEmail extends Mailable
{
    use Queueable, SerializesModels;

    protected $name;
    protected $email;
    protected $password;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($name, $email, $password)
    {
        $this->name = $name;
        $this->email = $email;
        $this->password = $password;

    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $date = Carbon::now()->year;
        return $this->view('email.email')
                    ->with([
                        'name_sol' => $this->name,
                        'email_sol' => $this->email,
                        'password_sol' => $this->password,
                        'date_sol' => $date,
                    ])
                    ->subject('Se ha Creado Tú Cuenta En La Platforma De CymacostaCode');
    }
}
