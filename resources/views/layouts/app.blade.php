<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- DataTable -->
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.23/css/jquery.dataTables.css" defer>

    <!-- Bootstrap-select -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/css/bootstrap-select.min.css">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <!-- Sweet Alert -->
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10" defer></script>
</head>
<body>
    {{-- text-light --}}
    <div id="app">
        <nav class="navbar color navbar-expand-md navbar-light shadow-sm">
            <div class="container">
                <a class="navbar-brand font-weight-bold" href="{{ url('/admin') }}">
                    {{ config('app.name', 'Laravel') }}
                </a>
                <button class="navbar-toggler btn-tam" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}" >
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div>
                    @php
                     // echo $route = Route::currentRouteName();
                      $routeAd = array('admin.index', 'admin.edit', 'admin.create');
                      $routePe = array('permission.index', 'permission.edit', 'permission.create');
                      $routeRo = array('perfil.index', 'perfil.edit', 'perfil.create');
                      $routeMo = array('module.index', 'module.edit', 'module.create');
                      /* {{ !Route::is('admin.index') ? 'text-dark':'text-white'}}  */
                    @endphp 
                 
                </div>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    {{-- <ul class="navbar-nav mr-auto">
                    </ul> --}}
                    @if(!Auth::guest())
                        <ul class="navbar-nav mr-auto mt-1">
                            @canany(['Visualizar', 'Crear', 'Editar', 'Eliminar', 'Visualizar.Admin', 'Crear.Admin' , 'Editar.Admin', 'Eliminar.Admin'])
                                <li>
                                    <a class="nav-link link-warning font-weight-bold 
                                        {{ in_array(Route::currentRouteName(), $routeAd) ? 'text-white':'text-dark' }}" 
                                        href="{{ route('admin.index') }}">Usuarios
                                    </a>
                                </li>
                            @endcanany

                            @canany(['Visualizar', 'Crear', 'Editar', 'Eliminar', 'Visualizar.Permiso', 'Crear.Permiso' , 'Editar.Permiso', 'Eliminar.Permiso'])
                                <li>
                                    <a class="nav-link link-warning font-weight-bold 
                                    {{ in_array(Route::currentRouteName(), $routePe) ? 'text-white':'text-dark' }}" href="{{ route('permission.index') }}">
                                        Permisos
                                    </a>
                                </li> 
                            @endcanany

                            @canany(['Visualizar', 'Crear', 'Editar', 'Eliminar', 'Visualizar.Perfil', 'Crear.Perfil' , 'Editar.Perfil', 'Eliminar.Perfil'])
                                <li>
                                    <a class="nav-link  link-warning font-weight-bold
                                    {{ in_array(Route::currentRouteName(), $routeRo) ? 'text-white':'text-dark' }}" 
                                    href="{{ route('perfil.index') }}">
                                        Perfiles
                                    </a> 
                                </li> 
                            @endcanany

                            @canany(['Visualizar', 'Crear', 'Editar','Eliminar', 'Visualizar.Modulo','Crear.Modulo' , 'Editar.Modulo','Eliminar.Modulo'])
                                <li>
                                    <a class="nav-link  link-warning font-weight-bold
                                    {{ in_array(Route::currentRouteName(), $routeMo) ? 'text-white':'text-dark' }}" 
                                    href="{{ route('module.index') }}">
                                        Module
                                    </a> 
                                </li>
                            @endcanany
                        </ul> 
                    @endif

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest
                            <li class="nav-item">
                                <a class="nav-link " href="{{ route('login') }}">{{ __('Login') }}</a>
                            </li>
                           {{--  @if (Route::has('register'))
                                <li class="nav-item ">
                                    <a class="nav-link " href="{{ route('register') }}">{{ __('Register') }}</a>
                                </li>
                            @endif --}}
                        @else
                            <div class="d-flex justify-content-start" >
                                @if(empty(Auth::user()->avatar))
                                    {{-- <img src="{{ asset('/storage/img/image.png') }}" class="rounded-circle img-avatar" style="width: 35px; height: 35px;"> --}}
                                    @php
                                        $name = Auth::user()->name;
                                        $last_name =  Auth::user()->last_name;
                                    @endphp
                                    <!--Api generadora de avatares con la el nombre y apellido -->
                                    <img src="https://ui-avatars.com/api/?name={{$name}}+{{$last_name}}&&background=C0B484&color=fff&size=128&font-size=0.46" class="rounded-circle img-avatar" style="width: 40px; height: 40px;"> 
                                @else
                                    <img src="{{  Auth::user()->avatar  }}" class="rounded-circle img-avatar" style="width: 40px; height: 40px;">
                                @endif
                            </div>
                            <li class="nav-item dropdown ">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle " href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }} {{ Auth::user()->last_name }}
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" 
                                       href="{{ route('userperfil.show',['id' => Auth::user()->id])}}">
                                       <i class="fas fa-user-alt"></i> {{ __('Ver Perfil') }}
                                    </a>
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        <i class="fas fa-sign-out-alt"></i> {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>

        <main class="py-4">
            @yield('content')
        </main>
        
    </div>
    @yield('scripts')

    <!-- Bootstrap-select -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/js/bootstrap-select.min.js" defer></script>
    
    <!-- DataTables -->
    <script src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js" defer></script>
    <script src="https://cdn.datatables.net/1.10.22/js/dataTables.bootstrap4.min.js" defer></script>
    <script src="https://cdn.datatables.net/responsive/2.2.6/js/dataTables.responsive.min.js" defer></script>
    <script src="https://cdn.datatables.net/responsive/2.2.6/js/responsive.bootstrap4.min.js" defer></script>
</body>
</html>
