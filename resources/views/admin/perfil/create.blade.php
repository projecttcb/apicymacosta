
@extends('layouts.app')


@canany(['Crear', 'Crear.Perfil'])
    @section('content')
        <div class="container ">
            <div class="row">
                <div class="card shadow" style="width: 100%">
                    <div class="card-body" >
                        <h2>Lista de Perfil</h2>
                        <br>
                            <p>1. Llene los campos que se encuentran a continuación y presione guardar para registrar el perfil.</p>
                        <br>
                        <form action="{{ route('perfil.store') }}" enctype="multipart/form-data" method="POST">
                            @csrf
                            @method('POST')
                            <div class="row">
                                <div class="col">
                                <label for="name">Perfil</label>
                                <input type="text" class="form-control   @error('name') is-invalid @enderror" name="name" placeholder="Nombre del Permisos" aria-label="First name">
                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                                </div>
                                <div class="col">
                                    <label for="guard_name">Tipo</label>
                                    <input type="text" class="form-control" name="guard_name" value="web"  aria-label="Last name" disabled>
                                </div>
                            </div>

                            <div class="row mt-4">
                                <div class="col-6">
                                    <label for="perfil">Permisoso</label>                               
                                    <select class="selectpicker" name="permission_id[]" title="Seleccionar..."  multiple data-actions-box="true" data-live-search="true" multiple data-selected-text-format="count > 3" data-width="100%" name="permission_id">
                                        @foreach($permission as $per)
                                            <option value="{{ $per->id }}"> {{ $per->name }} </option>
                                        @endforeach
                                    </select>                                
                                </div>
                            </div>
                            <button type="submit" class="btn-color mt-3 btn shadow" title="Guardar Permiso">
                                <i class="fas fa-save"></i> GUARDAR
                            </button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    @endsection
@endcanany
{{-- @endif --}}