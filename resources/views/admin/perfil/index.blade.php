@extends('layouts.app')
@php

  $arrayPer  = array();
  foreach (Auth::user()->permissions as $key => $value) {
    array_push($arrayPer, $value->name);
  }
    
@endphp 

@canany(['Visualizar', 'Crear', 'Editar', 'Eliminar', 'Visualizar.Perfil', 'Crear.Perfil' , 'Editar.Perfil', 'Eliminar.Perfil'])
  @section('content')
    <div class="container">
      <div class="row">
        <div class="card shadow" style="width: 100%">
          <div class="card-body" >
            <div class="d-flex justify-content-between">
                <h2 class="mt-2">Lista de Perfiles</h2>
                @canany(['Crear', 'Crear.Perfil'])
                  <a href="{{ route('perfil.create') }}" class="btn shadow-sm btn-color my-3">
                    <i class="far fa-plus-square"></i> CREAR 
                  </a>
                @endcanany
            </div>
            <br>
            <div class="table-responsive-sm">
              <table id="table_id"  class="table table-striped table-bordered hover " >
                <thead>
                  <tr class="text-center align-middle color">
                    <th scope="col">#</th>
                    <th scope="col">Permisos</th>
                    <th scope="col">Perfil</th>
                    @canany(['Visualizar', 'Visualizar.Perfil', 'Editar', 'Editar.Perfil', 'Eliminar', 'Eliminar.Perfil'])  
                      <th scope="col">Acciones</th>
                    @endcanany
                  </tr>
                </thead>
                <tbody>
                  @foreach($profiles as $pro)
                    <tr class="text-center align-middle ">
                      <th scope="row">{{ $pro->id }}</th>
                      <td>{{ $pro->name }}</td>
                      <td>
                        @if($pro->permissions->isNotEmpty())
                          @foreach($pro->permissions as $key => $value)
                            @if($key <= 1) 
                              <span class="badge bg-warning text-dark">{{ $value->name }}</span>
                            @endif
                          @endforeach
                          @if(!empty($key) && $key > 1)   
                            <span class="badge bg-warning align-middle" title="{{ ($key)-1 }} Permisos Más">
                              Más 
                            <span class="badge badge-light align-middle"> +{{ ($key)-1 }}</span>
                              <span class="sr-only">unread messages</span>
                            </span>    
                          @endif 
                        @else
                          <span class="badge align-middle badge-dark" >No Tiene <br> Permisos Asignados</span>
                          @endif
                      </td> 
                      @canany(['Visualizar', 'Visualizar.Perfil', 'Editar', 'Editar.Perfil', 'Eliminar', 'Eliminar.Perfil'])     
                        <td >
                          <div class="d-flex justify-content-center">
                            @canany(['Visualizar', 'Visualizar.Perfil'])
                              <button type="button"  
                                      class="btn btn-outline-info ml-1 mr-1 verPermission"id="{{ $pro->id }}" 
                                      data-toggle="modal" 
                                      data-target="#staticBackdrop" name="perfil"
                                      data-bs-toggle="tooltip"
                                      data-bs-placement="top"
                                      title="Ver Permission Asignado">
                                        <i class="far fa-eye"></i></i>
                              </button>
                            @endcanany

                            @canany(['Editar', 'Editar.Perfil'])
                              <a href="{{ route('perfil.edit', $pro->id) }}" class="btn btn-outline-success ml-1 mr-1" title="Editar Perfil">
                                <i class="fas fa-pencil-alt"></i>
                              </a>
                            @endcanany

                            @canany(['Eliminar', 'Eliminar.Perfil'])
                              <form action="{{ route('perfil.destroy', $pro->id) }}" method="POST">
                                @csrf
                                @method('DELETE')
                                <button type="submit"  class="btn btn-outline-danger ml-1 mr-1 formEliminar">
                                  <i class="far fa-trash-alt" title="Eliminar Perfil"></i>
                                </button>
                              </form>
                            @endcanany
                          </div>
                        </td>
                      @endcanany
                    </tr>
                  @endforeach 
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!--Modal-->
    @include('admin.ui.modal')
  @endsection

  @section('scripts')
    @if(session('eliminar') == 'ok')
        <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
        <script defer>
            Swal.fire(
                  'Eliminado!',
                  'El perfil ha sido eliminado.',
                  'success'
                ) 
        </script>
      @endif  
  @endsection
@endcanany
{{-- @endif  --}}

  