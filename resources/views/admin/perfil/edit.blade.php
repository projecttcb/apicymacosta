@extends('layouts.app')

@canany(['Editar', 'Editar.Perfil'])
    @section('content')
        <div class="container ">
            <div class="row">
                <div class="card shadow" style="width: 100%">
                    <div class="card-body" >
                        <h2>Lista de Perfil</h2>
                        <br>
                            <p>1. Llene los campos que se encuentran a continuación y presione actualizar para registrar los cambios en del perfil.</p>
                        <br>
                        <form action="{{ route('perfil.update', $profiles->id) }}" enctype="multipart/form-data" method="post">
                            @csrf
                            @method('PUT')
                            <div class="row">
                                <div class="col">
                                    <label for="name">Perfil</label>
                                    <input type="text" class="form-control   @error('name') is-invalid @enderror" name="name" placeholder="Nombre del Permisos" aria-label="First name" value="{{ $profiles->name }}">
                                    @error('name')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <div class="col">
                                    <label for="guard_name">Tipo</label>
                                    <input type="text" class="form-control" name="guard_name" value="web"   disabled>
                                </div>
                            </div>

                            <div class="row mt-4">
                                <div class="col-6">
                                    <label for="permission_id">Perfil</label>
                                    <select class="selectpicker" multiple data-actions-box="true" data-live-search="true"  title="Seleccionar..." multiple data-selected-text-format="count > 10" data-width="100%" name="permission_id[]">
                                        @foreach($permission as $per)
                                            <option value="{{ $per->id }}" {{ in_array($per->id, $permissionId) ? 'selected':''}}> 
                                                {{ $per->name }} 
                                            </option>
                                        @endforeach                            
                                    </select>
                                </div>
                            </div>
                            <button type="submit" class="btn-color mt-4 btn shadow" title="Actualizar">
                                <i class="fas fa-sync-alt mr-1" title="Actualizar"></i> ACTUALIZAR
                            </button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    @endsection
@endcanany
{{-- @endif --}}