@extends('layouts.app')

 @canany(['Visualizar', 'Crear', 'Editar', 'Eliminar',
          'Visualizar.Permiso', 'Crear.Permiso' , 
          'Editar.Permiso', 'Eliminar.Permiso'])
  @section('content')
    <div class="container">
      <div class="row">
        <div class="card shadow" style="width: 100%">
          <div class="card-body" >
            <div class="d-flex justify-content-between">
              <h2 class="mt-2">Lista de Permisos</h2>
              @canany(['Crear', 'Crear.Permiso'])
                <a href="{{ route('permission.create') }}" class="btn shadow-sm btn-color my-3" title="Crear Permisos">
                  <i class="far fa-plus-square"></i> CREAR
                </a>
              @endcanany
            </div>
            <br>
            <div class="table-responsive-sm">
              <table id="table_id"  class="table table-striped table-bordered hover" >
                <thead>
                  <tr class="text-center align-middle color">
                    <th scope="col">#</th>
                    <th scope="col">Permisos</th>
                    <th scope="col">Perfil</th>
                    @canany(['Visualizar', 'Visualizar.Permiso', 'Editar', 'Editar.Permiso', 'Eliminar', 'Eliminar.Permiso'])
                      <th scope="col">Acciones</th>
                    @endcanany
                  </tr>
                </thead>
                <tbody >
                  @foreach($permissions as $per)
                    <tr class="text-center align-middle">
                      <th  scope="row">{{ $per->id }}</th>
                      <td >{{ $per->name }}</td>
                      <td >
                        @if($per->roles->isNotEmpty())
                          @foreach($per->roles as $key => $perol)
                            @if($key <= 1) 
                              <span class="badge bg-warning text-dark align-middle">{{ $perol->name }}</span>
                            @endif
                          @endforeach
                          @if(!empty($key) && $key > 1)   
                            <span class="badge bg-warning align-middle" title="{{ ($key)-1 }} Permisos Más">
                              Más <span class="badge badge-light align-middle"> +{{ ($key)-1 }}</span>
                              <span class="sr-only">unread messages</span>
                            </span>    
                          @endif 
                        @else
                          <span class="badge bg-dark text-white">Sin Asignar</span>
                        @endif
                        
                      </td>
                      @canany(['Visualizar', 'Visualizar.Permiso', 'Editar', 'Editar.Permiso', 'Eliminar', 'Eliminar.Permiso'])
                        <td > 
                          <div class="d-flex justify-content-center">

                            @canany(['Visualizar', 'Visualizar.Permiso'])
                              <button class="btn btn-outline-info verPermission ml-1 mr-1 " id="{{ $per->id }}" data-toggle="modal" data-target="#staticBackdrop" name="permission" title="Ver Perfiles Asigando">
                                <i class="far fa-eye"></i>
                              </button>
                            @endcanany

                            @canany(['Editar', 'Editar.Permiso'])
                              <a href="{{ route('permission.edit', $per->id) }}" class="btn btn-outline-success ml-1 mr-1">
                                <i class="fas fa-pencil-alt" title="Editar Permisos"></i>
                              </a>
                            @endcanany

                            @canany(['Eliminar', 'Eliminar.Permiso'])
                              <form action="{{ route('permission.destroy', $per->id) }}" method="POST">
                                @csrf
                                @method('DELETE')
                                <button type="submit" class="btn btn-outline-danger formEliminar ml-1 mr-1 " id="permiso {{ $per->name }}" title="Eliminar Permisos">
                                  <i class="far fa-trash-alt"></i>
                                </button>
                              </form>
                            @endcanany
                          </div>
                        </td>
                      @endcanany
                    </tr>
                  @endforeach                      
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>

    {{-- Modal --}}
    @include('admin.ui.modal')

  @endsection

  @section('scripts')
    @if(session('eliminar') == 'ok')
      <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
      <script >
        Swal.fire(
          'Eliminado!',
          'El permiso ha sido eliminado.',
          'success'
        );
      </script>
    @endif
  @endsection
@endcanany
