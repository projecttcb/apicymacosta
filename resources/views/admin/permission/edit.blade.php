@extends('layouts.app')

@canany(['Editar', 'Editar.Permiso'])
    @section('content')
        <div class="container ">
            <div class="row">
                <div class="card shadow" style="width: 100%">
                    <div class="card-body" >
                        <h2>Lista de Permisos</h2>
                        <br>
                        <p>1. Llene los campos que se encuentran a continuación y presione actualizar para registrar los cambios en del permiso.</p>
                        <br>
                        <form action="{{ route('permission.update', $permission->id) }}" enctype="multipart/form-data" method="post">
                            @csrf
                            @method('PUT')
                            <div class="row">
                                <div class="col">
                                <label for="name">Permiso</label>
                                <input type="text" class="form-control @error('name') is-invalid @enderror" name="name" placeholder="Nombre del Permisos" aria-label="First name" value="{{ $permission->name }}">
                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                                </div>
                                <div class="col">
                                    <label for="guard_name">Tipo</label>
                                    <input type="text" class="form-control" name="guard_name" value="web"  aria-label="Last name" disabled>
                                </div>
                            </div>

                            <div class="row mt-4">
                                <div class="col">
                                    <label for="user">User</label>
                                    <select class="selectpicker" title="Seleccionar..."  multiple multiple data-actions-box="true" data-live-search="true"  data-selected-text-format="count > 10" data-width="100%" name="user_id[]" id="selectIdioma">
                                        @foreach($users as $user)
                                            <option value="{{ $user->id }}" {{ in_array($user->id, $userId) ? 'selected':''}}>
                                                {{ $user->name }} {{  $user->last_name }}
                                            </option>
                                        @endforeach
                                    </select>                                
                                </div>
                                <div class="col">
                                    <label for="perfil">Perfil</label>
                                    <select class="selectpicker" title="Seleccionar..." multiple data-selected-text-format="count > 10" data-width="100%" name="perfil[]">
                                        @foreach($roles as $rol)
                                            <option value="{{ $rol->id }}" {{ in_array($rol->id, $rolesId) ? 'selected':''}}>
                                                {{ $rol->name }} 
                                            </option>
                                        @endforeach                            
                                    </select>
                                </div>
                            </div>
                            <button type="submit" class="btn-color mt-3 btn shadow" title="Actualizar Permisos"> 
                                <i class="fas fa-sync-alt mr-1" ></i> ACTUALIZAR
                            </button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    @endsection
@endcanany
{{-- @endif --}}