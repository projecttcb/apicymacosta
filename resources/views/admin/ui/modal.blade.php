<!-- Modal -->
<div class="modal fade" id="staticBackdrop" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content" >
            <div class="modal-header card-header">
                <h3 class="modal-title justify-content-center d-flex" id="titleModal"></h3>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" >
                <div class="justify-content-center align-items-center" style="width: 100%; position: relative;">
                    <div class="justify-content-center">   
                        <div>  
                            @if( Request::url() == route('permission.index'))
                                <p class="text-muted ml-2">A continuación encontrará información de los perfiles asociado a los permisos</p>
                            @elseif(Request::url() == route('perfil.index'))                  
                                <p class="text-muted ml-2">A continuación encontrará información de los permisos asociados al perfil</p>
                            @elseif((Request::url() == route('admin.index')))
                                <p class="text-muted ml-2">A continuación encontrará información de los permisos asociados al Usuario</p>
                            @else
                                <p class="text-muted ml-2">A continuación encontrará información de los permisos asociados al Módulo</p>
                            @endif  
                                                            
                            <div id="tbody" class="text-left">
   
                            </div>

                            @if(Request::url() == route('admin.index') or Request::url() == route('module.index') )
                                <hr>
                                <p class="text-muted ml-2">A continuación encontrará información de los perfiles asociados al {{ (Request::url() == route('module.index')) ? 'Módulo':'Usuario' }}</p>
                                <div id="rol"></div> 
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
