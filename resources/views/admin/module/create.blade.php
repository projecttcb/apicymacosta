@extends('layouts.app')
@canany(['Crear', 'Crear.Modulo'])
    @section('content')
        <div class="container ">
            <div class="row">
                <div class="card shadow" style="width: 100%">
                    <div class="card-body" >
                        <h2>Crear de Módulo</h2>
                        <br>
                        <p>1. Llene los campos que se encuentran a continuación y presione guardar para registrar el Módulo.</p>
                        <br>
                        <form action="{{ route('module.store') }}" enctype="multipart/form-data" method="POST">
                            @csrf
                            @method('POST')
                            <div class="form-row">
                                <div>
                                    <label for="idModule" hidden id="labeIdModule">codigo</label>
                                    <input type="text" id="idModule" class="form-control  @error('idModule') is-invalid @enderror" name="idModule" value="{{ old('idModule') }}"  aria-label="First module" hidden>
                                    @error('idModule')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <div class="col">
                                    <label for="module">Módulo</label>
                                    <input type="text" id="module" class="form-control   @error('module') is-invalid @enderror" name="module" placeholder="Nombre del Permisos" aria-label="First module" >
                                    @error('module')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <div class="col">
                                <label for="guard_name">Tipo</label>
                                <select id="tipo" class="selectpicker  @error('module') is-invalid @enderror" title="Seleccionar..." name="tipo" data-width="100%">
                                        <option value="web">Web</option>
                                        <option value="movil">Móvil</option>
                                        <option value="ambos">Ambos</option>
                                    </select>
                                    @error('tipo')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror 
                                </div>
                            </div>
                            <div class="form-row mt-4">
                                <div class="col-md-6">
                                    <label for="permission_id">Permisos Directos</label>
                                    <select class="selectpicker" title="Seleccionar..."  multiple data-selected-text-format="count > 10" data-width="100%" data-actions-box="true" data-live-search="true" name="permission_id[]">
                                        @foreach($permission as $key => $per)
                                            <option value="{{ $per->id }}">{{ $per->name }}</option>
                                        @endforeach  
                                    </select>
                                </div>

                                <div class="col-md-6">
                                    <label for="permission_id">Perfiles</label>
                                    <select class="selectpicker" title="Seleccionar..."  multiple data-selected-text-format="count > 10" data-width="100%" name="perfil_id[]">
                                        @foreach($roles as $key => $rol)
                                            <option value="{{ $rol->id }}">{{ $rol->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <button type="submit" class="btn-color mt-3 btn shadow" title="Guardar Permisos">
                                <i class="fas fa-save mr-1"></i>GUARDAR
                            </button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    @endsection
@endcanany