@extends('layouts.app')

@canany(['Editar', 'Editar.Modulo'])
    @section('content')
        <div class="container ">
            <div class="row">
                <div class="card shadow" style="width: 100%">
                    <div class="card-body" >
                        <h2>Crear de Módulo</h2>
                        <br>
                        <p>1. Llene los campos que se encuentran a continuación y presione actualizar para registrar los cambios del Módulo.</p>
                        <br>
                        <form action="{{ route('module.update', $module->id) }}" enctype="multipart/form-data" method="POST">
                            @csrf
                            @method('PUT')
                            <div class="form-row">
                                <div>
                                    <label for="idModule" id="labeIdModule" {{ ($module->tipo != 'movil') ? 'hidden':'' }}>codigo</label>
                                    <input type="text" id="idModule" class="form-control  @error('idModule') is-invalid @enderror" name="idModule" value="{{ $module->idModule }}"  aria-label="First module" {{ ($module->tipo != 'movil') ? 'hidden':'' }}>
                                    @error('idModule')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <div class="col">
                                    <label for="module">Módulo</label>
                                    <input type="text" id="module" class="form-control   @error('module') is-invalid @enderror" name="module" placeholder="Nombre del Permisos" aria-label="First module" value="{{ $module->module }}">
                                    @error('module')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <div class="col">
                                <label for="guard_name">Tipo</label>
                                <select id="tipo" class="selectpicker  @error('module') is-invalid @enderror" title="Seleccionar..." name="tipo" data-width="100%">
                                        <option value="web"   {{ ($module->tipo == 'web') ? 'selected':''}}>Web</option>
                                        <option value="movil" {{ ($module->tipo == 'movil') ? 'selected':''}}>Móvil</option>
                                        <option value="ambos" {{ ($module->tipo == 'ambos') ? 'selected':''}}>Ambos</option>
                                    </select>
                                    @error('tipo')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror 
                                </div>
                            </div>
                            <div class="form-row mt-4">
                                <div class="col-md-6">
                                    <label for="permission_id">Permisos Directos</label>
                                    <select class="selectpicker" title="Seleccionar..."  multiple data-selected-text-format="count > 10" data-width="100%" data-actions-box="true" data-live-search="true" name="permission_id[]">
                                        @foreach($permission as $key => $per)
                                            <option value="{{ $per->id }}" {{ in_array($per->id, $idPermission) ? 'selected':''}}>{{ $per->name }}</option>
                                        @endforeach  
                                    </select>
                                </div>

                                <div class="col-md-6">
                                    <label for="permission_id">Perfiles</label>
                                    <select class="selectpicker" title="Seleccionar..."  multiple data-selected-text-format="count > 10" data-width="100%" name="perfil_id[]">
                                        @foreach($roles as $key => $rol)
                                            <option value="{{ $rol->id }}" {{ in_array($rol->id, $idRoles) ? 'selected':''}}>{{ $rol->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <button type="submit" class="btn-color mt-4 btn shadow" title="Guardar Permisos">
                                <i class="fas fa-save mr-1"></i>GUARDAR
                            </button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    @endsection
@endcanany