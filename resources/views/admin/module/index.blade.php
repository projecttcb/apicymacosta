@extends('layouts.app')

@canany(['Visualizar', 'Crear', 'Editar', 
        'Eliminar', 'Visualizar.Modulo', 
        'Crear.Modulo' , 'Editar.Modulo', 
        'Eliminar.Modulo'])
    @section('content')
        <div class="container">   
            <div class="row">
                <div class="card shadow" style="width: 100%">
                    <div class="card-body" >
                        <div class="d-flex justify-content-between">
                            <h2 class="mt-2">Lista de Módulos</h2>
                            @canany(['Crear', 'Crear.Modulo'])
                                <a href="{{ route('module.create') }}" class="btn shadow-sm btn-color my-3"><i class="far fa-plus-square"></i> CREAR </a>
                            @endcanany
                        </div>
                        <br>
                        <div class="table-responsive-lg">
                            <table id="table_id"  class="table table-striped table-bordered hover" >
                                <thead>
                                    <tr class="text-center align-middle color">
                                        <th scope="col">#</th>
                                        <th scope="col">Código</th>
                                        <th scope="col">Módulos</th>
                                        <th scope="col">Tipo</th>
                                        <th scope="col">Permisos</th>
                                        <th scope="col">Perfiles</th>
                                        @canany(['Visualizar', 'Visualizar.Modulo', 'Editar', 'Editar.Modulo', 'Eliminar', 'Eliminar.Modulo'])
                                            <th scope="col">Acciones</th>
                                        @endcanany
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($module as $key => $mod)
                                    <tr class="text-center align-middle">
                                        <td>{{ $mod->id }}</td>
                                        <td>
                                            @if($mod->idModule != null)
                                                {{ $mod->idModule }}
                                            @else
                                                <span class="badge bg-dark text-white">No tiene código</span>
                                            @endif
                                            
                                        </td>
                                        <td>{{ $mod->module }}</td>
                                        <td>{{ $mod->tipo }}</td>
                                        <td>
                                            @if($mod->permissions->isNotEmpty())
                                                
                                                @foreach($mod->permissions as $key => $per)
                                                    @if($key <= 1)
                                                        <span class="badge bg-warning text-dark align-middle"> {{ $per->name }} </span>
                                                    @endif
                                                @endforeach

                                                @if(!empty($key) && $key > 1)   
                                                    <span class="badge bg-warning align-middle" title="{{ ($key)-1 }} Permisos Más">
                                                    Más <span class="badge badge-light align-middle"> +{{ ($key)-1 }}</span>
                                                    <span class="sr-only">unread messages</span>
                                                    </span>    
                                                @endif 
                                                
                                            @else
                                                <span class="badge bg-dark text-white">Sin Asignar</span>
                                            @endif
                                        </td>
                                        <td>
                                            @if($mod->roles->isNotEmpty())
                                                @foreach($mod->roles as $key => $rol)
                                                    @if($key <= 1)
                                                        <span class="badge bg-warning text-dark align-middle">  {{ $rol->name }} </span>
                                                    @endif
                                                @endforeach
                                                @if(!empty($key) && $key > 1)   
                                                    <span class="badge bg-warning align-middle" title="{{ ($key)-1 }} Permisos Más">
                                                    Más <span class="badge badge-light align-middle"> +{{ ($key)-1 }}</span>
                                                    <span class="sr-only">unread messages</span>
                                                    </span>    
                                                @endif 
                                            @else
                                                <span class="badge bg-dark text-white">Sin Asignar</span>
                                            @endif
                                        </td>
                                        
                                        @canany(['Visualizar', 'Visualizar.Modulo', 'Editar', 'Editar.Modulo', 'Eliminar', 'Eliminar.Modulo'])
                                            <td > 
                                                <div class="d-flex justify-content-center">
                                                    @canany(['Visualizar', 'Visualizar.Modulo'])
                                                        <button class="btn btn-outline-info verPermission ml-1 mr-1 " id="{{ $mod->id }}" data-toggle="modal" data-target="#staticBackdrop" name="module" title="Ver Perfiles Asigando">
                                                            <i class="far fa-eye"></i>
                                                        </button>
                                                    @endcanany

                                                    @canany(['Editar', 'Editar.Modulo'])
                                                        <a href="{{ route('module.edit', $mod->id) }}" class="btn btn-outline-success ml-1 mr-1">
                                                            <i class="fas fa-pencil-alt" title="Editar Permisos"></i>
                                                        </a>
                                                    @endcanany

                                                    @canany(['Eliminar', 'Eliminar.Modulo'])
                                                        <form action="{{ route('module.destroy', $mod->id) }}" enctype="multipart/form-data" method="POST">
                                                            @csrf
                                                            @method('DELETE')
                                                            <button type="submit" class="btn btn-outline-danger formEliminar ml-1 mr-1 " id="module{{$mod->id}}" title="Eliminar Permisos">
                                                                <i class="far fa-trash-alt"></i>
                                                            </button>
                                                        </form>
                                                    @endcanany
                                                </div>
                                            </td>
                                        @endcanany
                                        
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--Modal-->
        @include('admin.ui.modal')
    @endsection

    @section('scripts')
        @if(session('eliminar') == 'ok')
            <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
            <script defer>
                Swal.fire(
                    'Eliminado!',
                    'El módulo ha sido eliminado.',
                    'success'
                ) 
            </script>
        @endif
    @endsection
@endcanany