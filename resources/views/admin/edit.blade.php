@extends('layouts.app')

@canany(['Editar', 'Editar.Admin'])
    @section('content')
        <div class="container">
            <div class="row">
                <div class="card shadow" style="width: 100%">
                    <div class="card-body" >
                        <h2>Crear de Usuario</h2>
                        <br>
                        <p>1. Llene los campos que se encuentran a continuación y presione guardar para registrar el usuario.</p>
                        <br>
                        <form action="{{ route('admin.update', $user->id) }}" enctype="multipart/form-data" method="POST">
                            @csrf
                            @method('PUT')
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label for="name">Nombre</label>
                                    <input type="text" class="form-control @error('name') is-invalid @enderror" id="name" name="name" value="{{ $user->name }}">
                                    @error('name')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="last_name">Apellidos</label>
                                    <input type="text" class="form-control @error('last_name') is-invalid @enderror" id="last_name" name="last_name"  value="{{ $user->last_name }}">
                                    @error('last_name')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="email">Correo</label>
                                <input type="email" class="form-control @error('email') is-invalid @enderror" id="email" name="email"  value="{{ $user->email }}">
                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <label for="permission_id">Permisos Directos</label>
                                    <select class="selectpicker" title="Seleccionar..."  multiple data-selected-text-format="count > 10" data-width="100%"  data-actions-box="true" data-live-search="true" name="permission_id[]">
                                        @foreach($permission as $key => $per)
                                            <option value="{{ $per->id }}" {{ in_array($per->id, $permissionId) ? 'selected':'' }}>{{ $per->name }}</option>
                                        @endforeach  
                                    </select>
                                </div>

                                <div class="col-md-6">
                                    <label for="permission_id">Perfiles</label>
                                    <select class="selectpicker" title="Seleccionar..."  multiple data-selected-text-format="count > 10"  data-actions-box="true" data-live-search="true" data-width="100%" name="perfil_id[]">
                                        @foreach($roles as $key => $rol)
                                            <option value="{{ $rol->id }}" {{ in_array($rol->id, $rolesId) ? 'selected':'' }}>{{ $rol->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-row mt-2 d-flex bd-highligh mr-3">                          
                                <div class="custom-control custom-checkbox ml-2 py-2 ">
                                    <input type="checkbox" class="custom-control-input" id="customCheck1" name="password_temp" value="1" {{ $passTempo != 0 ? 'checked':'' }}>
                                    <label class="custom-control-label" for="customCheck1">Activar Contraseña Temporal</label>
                                </div>   
                            </div>
                            
                            <div class="form-group mt-1">
                                <button type="submit" class="btn-color mt-3 btn shadow" title="Guardar Permisos">
                                    <i class="fas fa-sync-alt mr-1" ></i> ACTUALIZAR
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div> 
        </div>
    @endsection
@endcanany
