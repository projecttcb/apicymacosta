@extends('layouts.app')


@canany(['Crear', 'Crear.Admin'])
    @section('content')
        <div class="container">
            <div class="row">
                <div class="card shadow" style="width: 100%">
                    <div class="card-body" >
                        <h2>Crear de Usuario</h2>
                        <br>
                        <p>1. Llene los campos que se encuentran a continuación y presione guardar para registrar el usuario.</p>
                        <br>
                        <form action="{{ route('admin.store') }}" enctype="multipart/form-data" method="POST">
                            @csrf
                            @method('POST')
                            
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label for="name">Nombre</label>
                                    <input type="text" class="form-control  @error('name') is-invalid @enderror" id="name" name="name">
                                    @error('name')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>

                                <div class="form-group col-md-6">
                                    <label for="last_name">Apellidos</label>
                                    <input type="text" class="form-control  @error('last_name') is-invalid @enderror" id="last_name" name="last_name">
                                    @error('last_name')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="email">Correo</label>
                                <input type="email" class="form-control @error('last_name') is-invalid @enderror" id="email" name="email">
                                @error('last_name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <label for="permission_id">Permisos Directos</label>
                                    <select class="selectpicker" title="Seleccionar..."  multiple data-selected-text-format="count > 10" data-width="100%" data-actions-box="true" data-live-search="true" name="permission_id[]">
                                        @foreach($permission as $key => $per)
                                            <option value="{{ $per->id }}">{{ $per->name }}</option>
                                        @endforeach  
                                    </select>
                                </div>

                                <div class="col-md-6">
                                    <label for="permission_id">Perfiles</label>
                                    <select class="selectpicker" title="Seleccionar..."  multiple data-selected-text-format="count > 10" data-width="100%" name="perfil_id[]">
                                        @foreach($roles as $key => $rol)
                                            <option value="{{ $rol->id }}">{{ $rol->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group mt-3">
                            <button type="submit" class="btn-color mt-3 btn shadow" title="Guardar Permisos">
                                <i class="fas fa-save mr-1"></i>GUARDAR
                            </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div> 
        </div>
    @endsection
@endcanany
