@extends('layouts.app')

@canany(['Visualizar', 'Crear', 'Editar', 
        'Eliminar', 'Visualizar.Admin', 
        'Crear.Admin' , 'Editar.Admin', 
        'Eliminar.Admin'])
    @section('content')
        @php
            $arrayPer  = array();
            foreach (Auth::user()->permissions as $key => $value) {
                array_push($arrayPer,$value->name);
            }
        @endphp 
        <div class="container">   
            <div class="row">
                <div class="card shadow" style="width: 100%">
                    <div class="card-body" >
                        <div class="d-flex justify-content-between">
                            <h2 class="mt-2">Lista de Usuarios</h2>
                            @canany(['Crear', 'Crear.Admin'])
                                <a href="{{ route('admin.create') }}" class="btn shadow-sm btn-color my-3"><i class="far fa-plus-square"></i> CREAR </a>
                            @endcanany
                        </div>
                        <br>
                        
                        <div class="table-responsive-lg">
                            <table id="table_id"  class="table table-striped table-bordered hover" >
                                <thead>
                                    <tr class="text-center align-middle color">
                                        <th scope="col">#</th>
                                        <th scope="col">Avatar</th>
                                        <th scope="col">Usuarios</th>
                                        <th scope="col">Correo</th>
                                        <th scope="col">Permisos</th>
                                        <th scope="col">Perfiles</th>
                                        @canany(['Visualizar', 'Visualizar.Admin', 'Editar', 'Editar.Admin', 'Eliminar', 'Eliminar.Admin'])
                                            <th scope="col">Acciones</th>
                                        @endcanany
                                    </tr>
                                </thead>
                                <tbody >
                                    @foreach($users as $key => $user)
                                        <tr class="text-center align-middle">
                                            <th scope="row">{{ $user->id }}</th>
                                            <td >
                                                @if(empty($user->avatar))
                                                    
                                                <!--Api generadora de avatares con el nombre y apellido -->
                                                <img src="https://ui-avatars.com/api/?name={{$user->name}}+{{$user->last_name}}&background=C0B484&color=fff&size=128&font-size=0.46" class="rounded-circle img-avatar" style="width: 35px; height: 35px;"> 
                                                @else
                                                    <img src="{{  $user->avatar  }}" class="rounded-circle img-avatar" style="width: 35px; height: 35px;">
                                                @endif 
                                            </td>
                                            <td >   
                                                {{ $user->name }} {{ $user->last_name }}
                                            </td>                                            
                                            <td >{{ $user->email }} </td>
                                            <td >
                                                @if($user->permissions->isNotEmpty())
                                                    @foreach($user->permissions as $key => $userPer)
                                                        @if($key <=  1)
                                                            <span class="badge bg-warning text-dark align-middle">
                                                                {{ $userPer->name }}
                                                            </span>
                                                        @endif   
                                                    @endforeach
                                                    @if(!empty($key) && $key > 1)   
                                                        <span class="badge bg-warning align-middle" title="{{ ($key)-1 }} Permisos Más">
                                                        Más <span class="badge badge-light align-middle"> +{{ ($key)-1 }}</span>
                                                        <span class="sr-only">unread messages</span>
                                                        </span>    
                                                    @endif 
                                                @else
                                                    <span class="badge bg-dark text-white">Sin Asignar</span>
                                                @endif
                                            </td>
                                            <td >
                                                @if($user->roles->isNotEmpty())
                                                    @foreach($user->roles as $key => $userRol)
                                                        @if($key <= 1)
                                                            <span class="badge bg-warning text-dark align-middle">{{ $userRol->name }}</span>  
                                                        @endif
                                                        
                                                    @endforeach
                                                    @if(!empty($key) && $key > 1)   
                                                        <span class="badge bg-warning align-middle" title="{{ ($key)-1 }} Permisos Más">
                                                        Más <span class="badge badge-light align-middle"> +{{ ($key)-1 }}</span>
                                                        <span class="sr-only">unread messages</span>
                                                        </span>    
                                                    @endif 
                                                    
                                                @else
                                                    <span class="badge bg-dark text-white">Sin Asignar</span>
                                                @endif
                                            </td>
                                            @canany(['Visualizar', 'Visualizar.Admin', 'Editar', 'Editar.Admin', 'Eliminar', 'Eliminar.Admin'])
                                                <td >
                                                    <div class="d-flex justify-content-center">
                                                        
                                                        @canany(['Visualizar', 'Visualizar.Admin'])
                                                            <button class="btn btn-outline-info ml-1 mr-1  verPermission" id="{{ $user->id }}" data-toggle="modal" data-target="#staticBackdrop" name="admin" title="Ver Permisos Asigando">
                                                                <i class="far fa-eye"></i>
                                                            </button>
                                                        @endcanany

                                                        @canany(['Editar', 'Editar.Admin'])        
                                                            <a href="{{ route('admin.edit', $user->id) }}" class="btn btn-outline-success ml-1 mr-1" title="Editar Usuario">
                                                                <i class="fas fa-pencil-alt"></i>
                                                            </a>
                                                        @endcanany
                                                        
                                                        @canany(['Eliminar', 'Eliminar.Admin'])
                                                            <form action="{{ route('admin.destroy', $user->id) }}" method="POST">
                                                                @csrf
                                                                @method('DELETE')
                                                                <button type="submit"  class="btn btn-outline-danger formEliminar ml-1 mr-1 " id="permiso {{ $user->name }}" title="Eliminar Usuario"><i class="far fa-trash-alt"></i></button>
                                                            </form>
                                                        @endcanany
                                                    </div>
                                                </td>
                                            @endcanany
                                        </tr >
                                    @endforeach
                                    
                                </tbody >
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {{-- Modal --}}
        @include('admin.ui.modal')
    @endsection

    @section('scripts')

        @if(session('eliminar') == 'ok')
            <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
            <script defer>
                Swal.fire(
                    'Eliminado!',
                    'El usuario ha sido eliminado.',
                    'success'
                ) 
            </script>
        @endif
    @endsection

@endcanany
