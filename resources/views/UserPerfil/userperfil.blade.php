@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="card shadow" style="width: 100%">
            <div class="card-body" >
                <h2 >Mi Perfil </h2>
                <br>
                <form action="{{ route('userperfil.update', $user->id) }}" enctype="multipart/form-data" method="POST">
                    @csrf
                    @method('PUT')
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="name">Nombre</label>
                            <input type="text" class="form-control  @error('name') is-invalid @enderror" id="name" name="name" value="{{ Auth::user()->name }}">
                            @error('name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-group col-md-6">
                            <label for="last_name">Apellidos</label>
                            <input type="text" class="form-control  @error('last_name') is-invalid @enderror" id="last_name" name="last_name" value="{{ Auth::user()->last_name }}">
                            @error('last_name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
    
                    <div class="form-group">
                        <label for="email">Correo</label>
                        <input type="email" class="form-control @error('last_name') is-invalid @enderror" id="email" name="email" value="{{ Auth::user()->email }}" disabled>
                        @error('last_name')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="form-row">
                        <div class="form-group my-4 col-md-6" >
                            <label for="permission_id">Permisos: </label>
                            @if ( Auth::user()->permissions->isNotEmpty() == true)
                                @foreach ( Auth::user()->permissions as $permiso)
                                    <span class="badge badge badge-success">{{ $permiso->name }}</span>
                                @endforeach
                            @else
                                <span class="badge  badge badge-dark">No Tienes Permisos directos Asignado</span>
                            @endif
                              
                        </div>                      
                        <div class="form-group my-4  col-md-6">
                            <label for="roles_id">Roles: </label> 
                            @if ( Auth::user()->roles->isNotEmpty() == true)
                                @foreach ( Auth::user()->roles as $rol)  
                                     <span class="badge badge badge-success"> {{ $rol->name }} </span>
                                @endforeach
                            @else
                                <span class="badge  badge badge-dark">No Tienes Rol Asignado</span>
                            @endif                     
                            
                        </div>
                    </div>
                    
                    <div class="form-row">
                        <div class="form-group  col-md-6">
                            <label for="password" >{{ __('Password') }}</label>
                            <div class="input-group">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password"  autocomplete="new-password">
                                <div class="input-group-prepend">
                                    <button class="input-group-text btn" type="button" id="textPassword"><i class="far fa-eye-slash icon"></i></button>
                                </div> 
                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>                                
                        </div> 
                    
                        <div class="form-group  col-md-6">
                            <label for="password-confirm">{{ __('Confirm Password') }}</label>

                            {{-- <div class="col-md-6"> --}}
                                <div class="input-group">
                                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation"  autocomplete="new-password">
                                    
                                    <div class="input-group-prepend">
                                        <button class="input-group-text btn" type="button" id="textPassword2"><i class="far fa-eye-slash icon"></i></button>
                                    </div>
                                    
                                </div>
                            {{-- </div> --}}
                        </div>
                    </div> 

                    <div class="form-group mt-2">
                        <button type="submit" class="btn-color mt-3 btn shadow" title="Guardar Permisos">
                            <i class="fas fa-save mr-1"></i>GUARDAR
                        </button>
                    </div>
                </form>
                
            </div>
        </div>
    </div>
</div>

@endsection