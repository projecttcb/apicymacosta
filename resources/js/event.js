  //DataTable
/*   document.addEventListener('DOMContentLoaded', function() { */
    $(document).ready(function() {
      $('#table_id').DataTable({
        
        "language": {
                    "lengthMenu": "Mostrar " + 
                        `
                        <select class="custom-select mmb-select custom-select-sm form-control form-control-sm" style="width: 65px;">
                            <option value='10'>10</option>
                            <option value='25'>25</option>
                            <option value='20'>50</option>
                            <option value='100'>100</option>
                            <option value='-1'>Todos</option>
                        </select>

                        `
                    + " Resgristro",
                    "zeroRecords": "Nada encontrado - lo siento",
                    "info": "Mostrando la página _PAGE_ de _PAGES_",
                    "infoEmpty": "No records available",
                    "infoFiltered": "(filtrado de _MAX_ registro de totales)",
                    "search": "Buscar",
                    "paginate": {
                        'next': 'Seguiente',
                        'previous': 'Anterior'
                    }
        }
      });
    });
 /*  }); */

/* SweetAlert */
document.addEventListener('DOMContentLoaded', function(){
  $('.formEliminar').click(function(e) {
    e.preventDefault();
    let idBtn= $(this).attr("id")
    Swal.fire({
        title: '¿Estás seguro de eliminar el registro?',
        text : "Al eliminarlo no podrás volver a ver el registro!",
        icon : 'warning',
        showCancelButton  : true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor : '#d33',
        confirmButtonText : 'Si, bórralo!'
      }).then((result) => {
        if (result.isConfirmed) {
          $(this).parents('form').submit();
          
        }
      });
      
      
  });
});

//evento de ver
document.addEventListener('DOMContentLoaded', function() {
  $(document).ready(function() {
    $('.verPermission').click(function () {
      let id = $(this).attr("id");
      let name = $(this).attr("name");
      console.log(id);

      console.log($(this).attr("name"));
      if(id != ''){
        var element = '';
        var element2 = '';
        let urlAjax = '';
        let cont;  
        switch (name) {
          case 'permission':
            urlAjax =  '/ajaxPermission/' + id;
            cont = 1
            break;
          case 'perfil':
            urlAjax =  '/ajaxRoles/' + id;
            cont = 2;
            break;
          case 'admin':
            urlAjax = '/ajaxAdmin/'+id;
            cont = 3;
            break;
          case 'module':
            urlAjax = '/ajaxModule/'+id;
            cont = 4;
            break;
          default:
            break;
        }
        $.ajax({          
          url: urlAjax,
          type: "GET",
          dataType: "json",
          success: function (data) {
            if (cont == 1) {//ajaxPermisos
              var title = 'Perfiles Asignados a ' + data.permission;
            
              data.roles.forEach(el => {
                element +=  '<span class="badge color text-dark mt-1 ml-1 align-middle"><h6 class="align-middle font-weight-bold my-1"> ' + el + '</h6></span>'                          
              });

              $('#titleModal').html(title); 
              $('#tbody').html(element);

              if(data.roles.length == 0){
                var not = '<tr> <td> No Tiene Perfiles Asignados </td> </tr>'
                $('#tbody').html(not);
                //$('#tbody').load(" #tbody");
              }
            }else if(cont == 2){//ajaxPerfil
              
              var title = 'Permisos Asignados a ' + data.rol;

              data.permission.forEach(el => {
                element +=  '<span class="badge color text-dark mt-1 ml-1 "><h6 class="align-middle font-weight-bold my-1">' + el + '</h6></span>'
                   
              });

              $('#titleModal').html(title); 
              $('#tbody').html(element);

              if(data.permission.length == 0){
                var not = '<tr> <td> No Tiene Permisos Asignados </td> </tr>'
                $('#tbody').html(not);
                //$('#tbody').load(" #tbody");
              }
            }else if(cont == 3){ //ajaxAdmin
              
              var title = 'Permisos y perfiles asignados a ' + data.user;
              //console.log(data.permission.length);
              data.permission.forEach(el => {
                element +=  '<span class="badge color text-dark mt-1 ml-1 align-middle"><h6 class="align-middle font-weight-bold my-1 "> ' + el + '</h6></span>'
                              
              });

              data.role.forEach(el => {
                element2 +=  '<span class="badge color text-dark mt-1 ml-1 align-middle"><h6 class="align-middle font-weight-bold my-1 "> ' + el + '</h6></span>'
                //console.log(el);              
              });

              $('#titleModal').html(title); 
              $('#tbody').html(element);
              $('#rol').html(element2);
              
              if(data.permission.length == 0){
                var not = '<span class="badge align-middle badge-dark ml-2" > No tiene permisos directos asignados </span>'
                $('#tbody').html(not);
                //$('#tbody').load(" #tbody");
              }
              
              if( data.role.length == 0){
                var not2 = '<span class="badge align-middle badge-dark ml-2" > No tiene perfiles asignados </span>'
                $('#rol').html(not2);
                //$('#tbody').load(" #tbody");
              }
            }else{
              var title = 'Permisos Asignados al Módulo ' + data.module;

              data.permission.forEach(el => {
                element +=  '<span class="badge color text-dark mt-1 ml-1 "><h6 class="align-middle font-weight-bold my-1">' + el + '</h6></span>'       
              });
              
              data.role.forEach(el => {
                console.log(el);
                element2 +=  '<span class="badge color text-dark mt-1 ml-1 "><h6 class="align-middle font-weight-bold my-1">' + el + '</h6></span>'       
              });

              $('#titleModal').html(title); 
              $('#tbody').html(element);
              $('#rol').html(element2);
              
              if(data.permission.length == 0){
                var not = '<span class="badge align-middle badge-dark ml-2" >No Tiene Permisos Asignados </span>'
                $('#tbody').html(not);
                //$('#tbody').load(" #tbody");
              }

              if( data.role.length == 0){
                var not2 = '<span class="badge align-middle badge-dark ml-2" > No tiene perfiles asignados </span>'
                $('#rol').html(not2);
                //$('#tbody').load(" #tbody");
              }
            }
            
          },
          error: function(data){
          }
        });
      } 
    }); 
  });
});

//Ocultar y mostrar contraseña
$(document).ready(function () {
  $('#textPassword, #textPassword2').click(function () { 
      // let pass = $('#txtPassword').attr('type');
      let pass = $('#password').attr('type')
      let pass2 = $('#password-confirm').attr('type')
      let icono = $('.icon').attr('class')
      // console.log(pass2);
      if(pass == 'password' || pass2 == 'password' ){
          pass = $('#password').attr('type', 'text');
          pass2 = $('#password-confirm').attr('type', 'text');
          icono = $('.icon').attr('class', 'far fa-eye icon');

      }else{
          pass = $('#password').attr('type', 'password');
          pass2 = $('#password-confirm').attr('type', 'password');
          icono = $('.icon').attr('class', 'far fa-eye-slash icon');
      }

      //console.log(icono);
      //console.log(pass);
      
      //console.log(icono);
      //e.preventDefault();
      
  });
})
/*
$(document).ready(function () {
  $('#textPassword2').click(function () { 
      let pass = $('#password-confirm').attr('type');
      let icono = $('.icon2').attr('class')
      
      if(pass == 'password' ){
          pass = $('#password-confirm').attr('type', 'text');
          icono = $('.icon2').attr('class', 'far fa-eye icon2');
      }else{
          pass = $('#password-confirm').attr('type', 'password')
          icono = $('.icon2').attr('class', 'far fa-eye-slash icon2');
      }
      // console.log(pass);
     // console.log(icono); 
      
  });
}) 
*/


//cambiar el idioma al select
$(document).ready(function () {
  $('.show-tick').click(function() {
    let selectAll = $('.bs-select-all').html('Seleccionar todo');
    let deselectAll = $('.bs-deselect-all').html('Deseleccionar todo');
    //console.log(slectAll);
  });
});

/* $(document).ready(function () {
  $('.bootstrap-select').click(function() {
    let selectAll = $('.bs-select-all').html('Seleccionar todo');
    let deselectAll = $('.bs-deselect-all').html('Deseleccionar todo');
    //console.log(slectAll);
  });
}); */

$(document).ready(function () {
  $('#tipo').on('change', function() {
    let tipo = $(this).val();

    if(tipo == 'movil'){
      $('#labeIdModule').removeAttr('hidden');
      $('#idModule').removeAttr('hidden');
      $('#idModule').addClass('col');
    }else if(tipo == 'web' || tipo == 'ambos'){
      $('#idModule').attr('hidden', 'hidden');
      $('#labeIdModule').attr('hidden', 'hidden');
      //console.log(hiden);
    }
    


  });
});
